(use-modules (srfi srfi-41))

(define fib
  (stream-cons 0 (stream-cons 1 (stream-map + fib (stream-cdr fib)))))

(define even-fib
  (stream-filter even? fib))

(stream-fold + 0 (stream-take-while (lambda (x) (< x 4000000)) even-fib))
