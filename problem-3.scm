(define (divisible? m n)
  (zero? (modulo m n)))

(define (largest-prime-factor n)
  (define (inner n d)
    (cond ((= d n)
           d)
          ((divisible? n d)
           (inner (/ n d) d))
          (else
           (inner n (1+ d)))))
  (inner n 2))

(largest-prime-factor 600851475143)
