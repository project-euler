(use-modules (srfi srfi-1)
             (srfi srfi-42))

(define (products min max)
  (list-ec (:range x min max) (:range y min max) (* x y)))

(define (palindrome? x)
  (define (test s)
    (cond ((or (string-null? s)
               (= (string-length s) 1))
           #t)
          ((equal? (string-ref s 0)
                   (string-ref s (1- (string-length s))))
           (test (substring s 1 (1- (string-length s)))))
          (else
           #f)))
  (let ((s (number->string x)))
    (test s)))

(define (palindrome-products min max)
  (filter palindrome? (products min max)))

(reduce max 0 (palindrome-products 100 1000))
