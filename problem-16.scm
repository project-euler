;;;
;;; Problem 16: Power digit sum
;;;

;;; https://projecteuler.net/problem=16

(define (digit-sum n)
  (let loop ((m   1)
             (sum 0))
    (if (> (expt 10 (1- m)) n)
        sum
        (loop (1+ m) (+ sum (digit m n))))))

(define (digit m n)
  "Return the Mth digit of N."
  ;; Tenths place, hundreds place, etc.
  (let ((place (expt 10 (1- m))))
    (/ (- (modulo n (expt 10 m))
          (modulo n place))
       place)))

(digit-sum (expt 2 1000))
