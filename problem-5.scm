(use-modules (srfi srfi-1))

(define (divisible? m n)
  (zero? (modulo m n)))

(define (divisible-by-all? min max x)
  (define (inner x n)
    (cond ((= n max)
           #t)
          ((divisible? x n)
           (inner x (1+ n)))
          (else
           #f)))
  (inner x min))

(define (smallest-multiple min max)
  (define (inner n)
    (if (divisible-by-all? min max n)
        n
        (inner (1+ n))))
  (inner 1))

;; WARNING: Takes a long time.
(smallest-multiple 1 21)
