(use-modules (srfi srfi-1))

(define (divisible? m n)
  (zero? (modulo m n)))

(define (divisible-by-any? x nums)
  (any (lambda (n)
         (divisible? x n))
       nums))

(define (nth-prime n)
  (define (inner m count primes)
    (cond ((= count n)
           (car primes))
          ((divisible-by-any? m primes)
           (inner (1+ m) count primes))
          (else
           (inner (1+ m) (1+ count) (cons m primes)))))
  (inner 2 0 '()))

(nth-prime 10001)
