(use-modules (srfi srfi-41))

(define (square x)
  (* x x))

(define naturals (stream-from 1))

(define squares
  (stream-map square naturals))

(define (sum-stream n stream)
  (stream-fold + 0 (stream-take n stream)))

(define (sum-square-difference n)
  (- (square (sum-stream n naturals))
     (sum-stream n squares)))

(sum-square-difference 100)
