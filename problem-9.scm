(use-modules (srfi srfi-1)
             (srfi srfi-42))

(define (square x)
  (* x x))

;; Euclid's formula.
(define (make-triple n m)
  (list (- (square n) (square m))
        (* 2 n m)
        (+ (square n) (square m))))

(define (generate-triples max-n)
  (concatenate
   (list-ec (:range n 2 max-n)
            (list-ec (:range m 1 (1- n))
                     (make-triple n m)))))

(define (find-triple-sum goal)
  (find (lambda (triple)
          (= (apply + triple) goal))
        ;; Assumed that a range of 50 would be enough.
        (generate-triples 50)))

(apply * (find-triple-sum 1000))
