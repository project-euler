(use-modules (srfi srfi-1))

(define (divisible? m n)
  (zero? (modulo m n)))

(define (prime-sieve n)
  (define (mark-multiples! bits x)
    (define (mark y)
      (when (< y n)
        (bitvector-set! bits (1- y) #t)
        (mark (+ y x))))
    (mark x))
  (define (sieve m primes bits)
    (cond ((= m n)
           primes)
          ((bitvector-ref bits (1- m))
           (sieve (1+ m) primes bits))
          (else
           (mark-multiples! bits m)
           (sieve (1+ m) (cons m primes) bits))))
  (sieve 2 '() (make-bitvector n)))

(reduce + 0 (prime-sieve 2000000))
